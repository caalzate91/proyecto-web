/**
 * 
 */
package com.premize.proyectos.business;

import java.util.Date;
import java.util.List;

import com.premize.proyecto.entidades.Usuarios;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class BusinessLogicAPI
 * @since 27/09/2015
 *
 */
public interface BusinessLogicAPI {
	
	public List<Usuarios> selectDataCedula() throws Exception;
	public void insertData(String tokenFormHTML, int validUser, int valCedulaUser, String valNombreUsuario, Date valFecha, int valTelefono) throws Exception;
	
}
