/**
 * 
 */
package com.premize.proyectos.business;

import java.util.Date;
import java.util.List;

import com.premize.proyecto.dao.UsuarioDAOApi;
import com.premize.proyecto.dao.UsuarioDAOImplement;
import com.premize.proyecto.entidades.Usuarios;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class BussinessLogic
 * @since 27/09/2015
 *
 */
public class BussinessLogic implements BusinessLogicAPI{

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 27/09/2015
	 * @param args
	 * @throws Exception 
	 */
	
	//Variables para validar
	
	static String tokenLogin = "12a1wj8756vm74db7";
	String tokenForm         = "9bj59ngft479hfde2";
	
	//Variables para insert
	//int validUser = 3;
	//int valCedulaUser = 567890123;
	//StringBuilder valNombreUsuario = new StringBuilder("Consiafirulo Velez");
	//String valNombreUsuario = "Consiafirulo Velez";
	//Date valFecha = (Date) Calendar.getInstance().getTime();
	//int valTelefono = 4567890;

	
	public List<Usuarios> selectDataCedula() throws Exception {
		//Validamos el token
		if ( this.tokenForm == "9bj59ngft479hfde2" ){
			
			//instaciamos el DAO de usuario
			UsuarioDAOApi usuarioAPI = new UsuarioDAOImplement();
			//Asignamos un objeto para traer la cedula
			Usuarios cedula = usuarioAPI.findCedula(1);
			
			//Validamos si en nuestra tabla hay registros.
			if ( cedula == null ) {
				//Imprimimos los datos 
				return usuarioAPI.getAllUsers(cedula);
			} else {
				//Sacamos la excepcion.
				throw new IllegalArgumentException("No hay datos que imprimir. Por favor revisar los registros. ");
			}
		} else {
			
			//Sacamos la excepcion.
			throw new IllegalArgumentException("EL token de seguridad no coinciden.");
		}
	}

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 27/09/2015
	 * @see com.premize.proyectos.business.BusinessLogicAPI#insertData(int, int, java.lang.String, java.sql.Date, int)
	 */
	public void insertData(String tokenFormHTML, int validUser, int valCedulaUser,
			String valNombreUsuario, Date valFecha, int valTelefono)
			throws Exception {
		//Validamos el token
		if ( this.tokenForm.equals(tokenFormHTML)){
			
			//Realizamos la validacion
			
			if ( validUser != 0 && valNombreUsuario != null ) {
				
				//Realizamos un registro en la base de datos
				Usuarios usuarios = new Usuarios();
				UsuarioDAOApi usuarioAPI = new UsuarioDAOImplement();
				usuarios.setIdUser(validUser);
				usuarios.setCedulaUser(valCedulaUser);
				usuarios.setNombreUser(valNombreUsuario);
				usuarios.setFechaNacimientoUser(valFecha);
				usuarios.setTelefonoUser(valTelefono);
				usuarioAPI.saveData(usuarios);
			} else {
				//Sacamos la excepcion.
				throw new IllegalArgumentException("No hay datos que imprimir. Por favor revisar los registros. ");
			}
		} else {
			
			//Sacamos la excepcion.
			throw new IllegalArgumentException("El token de seguridad no coinciden.");
		}
		
	}

}
