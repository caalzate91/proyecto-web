/**
 * 
 */
package com.premize.proyecto.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class Usuarios
 * @since 26/09/2015
 *
 */

@Entity
@Table(name="users")
public class Usuarios {
	
		@Id
		@Column(name="idUser")
		private int idUser;
		
		@Column(name="cedulaUser")
		private int cedulaUser;

		@Column(name="nombreUser")
		private String nombreUser;

		@Column(name="fechaNacimientoUser")
		private Date fechaNacimientoUser;
		
		@Column(name="telefonoUser")
		private int telefonoUser;

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @return the idUser
		 */
		public int getIdUser(){
			return idUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @param idUser the idUser to set
		 */
		public void setIdUser(int idUser) {
			this.idUser = idUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @return the cedulaUser
		 */
		public int getCedulaUser() {
			return cedulaUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @param cedulaUser the cedulaUser to set
		 */
		public void setCedulaUser(int cedulaUser) {
			this.cedulaUser = cedulaUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @return the nombreUser
		 */
		public String getNombreUser() {
			return nombreUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @param nombreUser the nombreUser to set
		 */
		public void setNombreUser(String nombreUser) {
			this.nombreUser = nombreUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @return the fechaNacimientoUser
		 */
		public Date getFechaNacimientoUser() {
			return fechaNacimientoUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @param fechaNacimientoUser the fechaNacimientoUser to set
		 */
		public void setFechaNacimientoUser(Date fechaNacimientoUser) {
			this.fechaNacimientoUser = fechaNacimientoUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @return the telefonoUser
		 */
		public int getTelefonoUser() {
			return telefonoUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @param telefonoUser the telefonoUser to set
		 */
		public void setTelefonoUser(int telefonoUser) {
			this.telefonoUser = telefonoUser;
		}

		/**
		 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
		 * @since 26/09/2015
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Usuarios [idUser=" + idUser + ", cedulaUser=" + cedulaUser
					+ ", nombreUser=" + nombreUser + ", fechaNacimientoUser="
					+ fechaNacimientoUser + ", telefonoUser=" + telefonoUser
					+ "]";
		}
		
		
	
}
