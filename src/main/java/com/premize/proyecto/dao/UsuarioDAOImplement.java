/**
 * 
 */
package com.premize.proyecto.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.premize.proyecto.entidades.Usuarios;
import com.premize.proyecto.util.HibernateUtil;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class UsuarioDAOImplement
 * @since 26/09/2015
 *
 */
public class UsuarioDAOImplement implements UsuarioDAOApi {

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 26/09/2015
	 * @see com.premize.proyecto.dao.UsuarioDAOApi#saveData(com.premize.proyecto.entidades.Usuarios)
	 */
	public void saveData(Usuarios usuarios) throws Exception{
		
		// Colocamos la variable en Nulo
		Transaction trans = null;
		
		//Creamos session 
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		try{
			//Realizamos transacción de guardado de archivos
			trans = session.beginTransaction();
			session.save(usuarios);
			session.getTransaction().commit();
		}
		catch(RuntimeException e) {
			
			//Si hubo error en la transaccion realizamos un rollback
			if ( trans != null ) {
				trans.rollback();
			}
			
			throw e;
			
		}
		finally {
			//Cerramos sesion despues de todo la ejecuccion
			session.flush();
			session.close();
		}
		
	}

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 26/09/2015
	 * @see com.premize.proyecto.dao.UsuarioDAOApi#deleteData(com.premize.proyecto.entidades.Usuarios)
	 */
	public void deleteData(int idUser) throws Exception{
		// Colocamos la variable en Nulo
		Transaction trans = null;
		
		//Creamos sesion 
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trans = session.beginTransaction();
			Usuarios usuarios = (Usuarios) session.load(Usuarios.class, new Integer(idUser));
			session.delete(usuarios);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			
			if (trans != null) {
                trans.rollback();
            }
			throw e;
			
		} finally {
			session.flush();
            session.close();
		}
		
	}

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 26/09/2015
	 * @see com.premize.proyecto.dao.UsuarioDAOApi#updateData(com.premize.proyecto.entidades.Usuarios)
	 */
	public void updateData(Usuarios usuarios) throws Exception{
		// Colocamos la variable en Nulo
		Transaction trans = null;
		
		//Creamos session 
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		try{
			//Realizamos transacción de guardado de archivos
			trans = session.beginTransaction();
			session.update(usuarios);
			session.getTransaction().commit();
		}
		catch(RuntimeException e) {
			
			//Si hubo error en la transaccion realizamos un rollback
			if ( trans != null ) {
				trans.rollback();
			}
			
			throw e;
			
		}
		finally {
			//Cerramos sesion despues de todo la ejecuccion
			session.flush();
			session.close();
		}
		
	}

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 26/09/2015
	 * @see com.premize.proyecto.dao.UsuarioDAOApi#getAllUsers(com.premize.proyecto.entidades.Usuarios)
	 */
	public List<Usuarios> getAllUsers(Usuarios usuarios) throws Exception{
		
		List<Usuarios> usuariosList = new ArrayList<Usuarios>();
		// Colocamos la variable en Nulo
		Transaction trans = null;
        
        //Creamos session 
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
        	//Realizamos transacción de listado 
        	trans = session.beginTransaction();
            usuariosList = session.createQuery("from Usuarios").list();
            
        } catch (RuntimeException e) {
        	throw e;
        } finally {
            session.flush();
            session.close();
        }
        return usuariosList;
	}

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @return 
	 * @return 
	 * @since 27/09/2015
	 * @see com.premize.proyecto.dao.UsuarioDAOApi#findCedula(int)
	 */
	public Usuarios findCedula(int cedulaUser) throws Exception {
		
		// Colocamos la variable en Nulo
		Usuarios usuarios = null;
		Transaction trans = null;
		String queryWhere = null;
        
        //Creamos session 
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
        	//Realizamos transacción de listado 
        	trans = session.beginTransaction();
        	queryWhere = "from Usuarios where cedulaUser = :cedulaUser";
            Query query = session.createQuery(queryWhere);
            query.setInteger("cedulaUser", cedulaUser);
            usuarios = (Usuarios) query.uniqueResult();
            
        } catch (RuntimeException e) {
        	throw e;
        } finally {
            session.flush();
            session.close();
        }
        return usuarios;
	}



}
