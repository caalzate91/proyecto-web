/**
 * 
 */
package com.premize.proyecto.dao;

import java.util.List;

import com.premize.proyecto.entidades.Usuarios;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class UsuarioDAOApi
 * @since 26/09/2015
 *
 */
public interface UsuarioDAOApi {
	
	public void saveData(Usuarios usuarios) throws Exception;
	public void deleteData(int idUser) throws Exception;
	public void updateData(Usuarios usuarios) throws Exception;
	public List<Usuarios> getAllUsers(Usuarios usuarios) throws Exception;
	public Usuarios findCedula(int cedulaUser) throws Exception;

}
