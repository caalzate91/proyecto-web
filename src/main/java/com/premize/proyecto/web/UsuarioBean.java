/**
 * 
 */
package com.premize.proyecto.web;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.premize.proyectos.business.BussinessLogic;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class UsuarioBean
 * @since 27/09/2015
 *
 */
@ViewScoped
@ManagedBean
public class UsuarioBean {
	
	private String tokenFormHTML;
	private int idUserInsert;
	private int cedula;
	private String nombre;
	private Date fechaNacimiento;
	private int telefono;

	@PostConstruct
	public void init() {
		tokenFormHTML = "9bj59ngft479hfde2";
		idUserInsert = 3;
	}

	public void guardar() {
		
		// Creamos los datos
		BussinessLogic business = new BussinessLogic();
		try {
			//business.insertData(tokenFormHTML, idUserInsert, cedula, nombre, fechaNacimiento, telefono);
			
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Successful",
			"Has creado los siguientes : ID: "+ idUserInsert + " Cedula: "+ cedula + " Nombre :" + nombre + " Fecha Nacimiento : "+ fechaNacimiento + " Telefono : " + telefono));
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			
			context.addMessage(null, new FacesMessage("Error", e.getMessage()));
		}
				
	}
	
	/*
	 * 
	 */

	public void recibirFormulario() {
		System.out.println("Entre");
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getCedula() {
		return cedula;
	}

	public void setCedula(int cedula) {
		this.cedula = cedula;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getTokenFormHTML() {
		return tokenFormHTML;
	}

	public void setTokenFormHTML(String tokenFormHTML) {
		this.tokenFormHTML = tokenFormHTML;
	}

	public int getIdUserInsert() {
		return idUserInsert;
	}

	public void setIdUserInsert(int idUserInsert) {
		this.idUserInsert = idUserInsert;
	}

}
