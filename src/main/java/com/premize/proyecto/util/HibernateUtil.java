/**
 * 
 */
package com.premize.proyecto.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class HibernateUtil
 * @since 26/09/2015
 *
 */
public class HibernateUtil {
	
	private static final SessionFactory sessionFactory = buildSessionFactory();

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 26/09/2015
	 * @return
	 */
	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		}
		catch(Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
	
	

	public static Session getSession() throws HibernateException {
		Session sess = null;
		try {
			sess = sessionFactory.getCurrentSession();
		} catch (org.hibernate.HibernateException he) {
			sess = sessionFactory.openSession();
		}
		return sess;
	}

}
