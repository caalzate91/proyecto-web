/**
 * 
 */
package com.premize.proyecto.test;


import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import com.premize.proyecto.dao.UsuarioDAOApi;
import com.premize.proyecto.dao.UsuarioDAOImplement;
import com.premize.proyecto.entidades.Usuarios;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class UsuarioTest
 * @since 26/09/2015
 *
 */	
public class UsuarioTest {

	@Test
	public void test() throws Exception {
		
		try {
			
			UsuarioDAOApi usuarioTest = new UsuarioDAOImplement();
			//Agregar Usuario
			Usuarios usuarios = new Usuarios();
			/*usuarios.setIdUser(2);
			usuarios.setCedulaUser(987654321);
			usuarios.setNombreUser("Pepita Perez");
			usuarios.setFechaNacimientoUser(Calendar.getInstance().getTime());
			usuarios.setTelefonoUser(567890);
			usuarioTest.saveData(usuarios);*/
			
			//Listar Usuarios
			//System.out.println(usuarioTest.getAllUsers(usuarios));
			
			//borrar usuario
			usuarioTest.deleteData(2);
			
			//Actualizar usuario
			//usuarioTest.updateData(usuarios);
			
			//Buscar si existe la cedula
			//System.out.println(usuarioTest.findCedula(1234567890));
		} catch ( Exception e ){
			Assert.fail();
		}
	}
}
