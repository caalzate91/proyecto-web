/**
 * 
 */
package com.premize.proyecto.test;

import java.util.Calendar;

import com.premize.proyecto.dao.UsuarioDAOApi;
import com.premize.proyecto.dao.UsuarioDAOImplement;
import com.premize.proyecto.entidades.Usuarios;

/**
 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
 * @project proyecto-web
 * @class UsuarioPrueba
 * @since 26/09/2015
 *
 */
public class UsuarioPrueba {

	/**
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 26/09/2015
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		UsuarioDAOApi usuarioTest = new UsuarioDAOImplement();
		
		//Agregar Usuario
		Usuarios usuarios = new Usuarios();
		/*usuarios.setIdUser(1);
		usuarios.setCedulaUser(1234567890);
		usuarios.setNombreUser("Camilo Alzate");
		usuarios.setFechaNacimientoUser(Calendar.getInstance().getTime());
		usuarios.setTelefonoUser(123456);
		usuarioTest.saveData(usuarios);*/
		
		//Listar Usuarios
		System.out.println(usuarioTest.getAllUsers(usuarios));

	}

}
